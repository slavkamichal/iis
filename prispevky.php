<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Lekarna</title> 
<link rel="stylesheet" href="style.css" type="text/css" >
</head>

<body>

    <?php                             
        require_once 'mainInit.php';
    ?>
    <?php
        $poist = 0  ;
        $liek  = 0;
        $tabulka = "";
        if (array_key_exists('poist', $_GET)){
            $poist = $_GET['poist'];
            $tabulka = "SELECT  LIEK.nazov AS  `liek` , POISTOVNA.nazov AS  `poistovna` , DOPLATOK.vyska AS  `doplatok` 
                    FROM LIEK
                    NATURAL JOIN DOPLATOK
                    JOIN POISTOVNA
                    WHERE POISTOVNA.id_poistovne = DOPLATOK.id_poistovne
                    AND LIEK.predpis = 1
                    AND DOPLATOK.id_poistovne =".$poist;
        }
        if (array_key_exists('liek', $_GET)){
            $liek = $_GET['liek'];
            if ($liek != 0){
                $tabulka = "SELECT LIEK.nazov AS  `liek` , POISTOVNA.nazov AS  `poistovna` , DOPLATOK.vyska AS  `doplatok` 
                        FROM LIEK
                        NATURAL JOIN DOPLATOK
                        JOIN POISTOVNA
                        WHERE POISTOVNA.id_poistovne = DOPLATOK.id_poistovne
                        AND LIEK.predpis = 1
                        AND DOPLATOK.id_liek =".$liek;
            }
        }

        if ($poist != 0 && $liek != 0){
            $tabulka = "SELECT LIEK.nazov AS  `liek` , POISTOVNA.nazov AS  `poistovna` , DOPLATOK.vyska AS  `doplatok` 
                    FROM LIEK
                    NATURAL JOIN DOPLATOK
                    JOIN POISTOVNA
                    WHERE POISTOVNA.id_poistovne = DOPLATOK.id_poistovne
                    AND LIEK.predpis = 1
                    AND DOPLATOK.id_liek =".$liek."
                    AND DOPLATOK.id_poistovne =".$poist;
        }
        if ($poist)
    ?>
	<script>
    function showLIEK(url) {
        document.location.href=url;
    }
	</script>

    <div id="main">
        <?php include 'header.php' ?>
    	  <div id="content">
            <form action="prispevky.php" method="GET">
                Vyberte poistovnu:
                <?php
                    echo "<select name='poist'>";
                    $sql= "SELECT * FROM  `POISTOVNA` ";

                    if (! $result = mysql_query($sql)){
                        die('error');
                    }

                    echo "<option value='0' selected>Ziadna</oprion>";
                    while($row = mysql_fetch_assoc($result)) {
                        if ($row['ID_POISTOVNE'] != $poist && $row['ID_POISTOVNE'] != 0){
                            echo '<option value='.$row['ID_POISTOVNE'].'>'.$row['ID_POISTOVNE'].$row['NAZOV'].'</option>';
                        }
                        else if ($poist != 0 && $row['ID_POISTOVNE'] != 0)
                            echo '<option selected value='.$row['ID_POISTOVNE'].'>'.$row['NAZOV'].'</option>';

                    }
                    echo '</select>';
                ?>
                Vyberte liek:
                <?php
                    echo "<select name='liek'>";


                    $sql= "SELECT `id_liek`, `nazov` FROM  `LIEK`";

                    if (! $result = mysql_query($sql)){
                        die('error');
                    }

                    echo "<option value='0' selected>Ziaden</oprion>";
                    while($row = mysql_fetch_assoc($result)) {
                        if ($row['id_liek'] != $liek){
                            echo '<option value='.$row['id_liek'].'>'.$row['nazov'].'</option>';
                        }
                        else if ($liek != 0)
                            echo '<option selected value='.$row['id_liek'].'>'.$row['nazov'].'</option>';
                    }
                    echo '</select>';
                ?>
                <input type="submit" value="Zobrazit">
            </form>
            <table id="Table">
            <tr>
                <th>Liek</th>
                <th>Poistovna</th>
                <th>Doplatok</th>
            </tr>

            <?php
                if (! $result = mysql_query($tabulka)){
                    die();
                }
                if (!$row = mysql_fetch_assoc($result)){
                    echo '<tr class="TableRow" 
                    onmouseover=\'changeRowColor(this, true)\' 
                    onmouseout=\'changeRowColor(this, false)\' '.
                    // onclick=\'showLIEK("lek.php?id='.$row['ID_LIEK'].'")\'
                    '>';
                    echo '<td>-</td>';
                    echo '<td>-</td>';
                    echo '<td>-</td>';
                    echo '</tr>';
                }
                else{
                    echo '<tr class="TableRow" 
                    onmouseover=\'changeRowColor(this, true)\' 
                    onmouseout=\'changeRowColor(this, false)\' '.
                    // onclick=\'showLIEK("lek.php?id='.$row['ID_LIEK'].'")\'
                    '>';
                    echo '<td>' . $row['liek'] . '</td>';
                    echo '<td>' . $row['poistovna'] . '</td>';
                    echo '<td>'.$row['doplatok'].'</td>';
                    echo '</tr>';
                }

                while($row = mysql_fetch_assoc($result)) {
                    echo '<tr class="TableRow" 
                    onmouseover=\'changeRowColor(this, true)\' 
                    onmouseout=\'changeRowColor(this, false)\' '.
                    // onclick=\'showLIEK("lek.php?id='.$row['ID_LIEK'].'")\'
                    '>';
                    echo '<td>' . $row['liek'] . '</td>';
                    echo '<td>' . $row['poistovna'] . '</td>';
                    echo '<td>'.$row['doplatok'].'</td>';
                    echo '</tr>';
                }
            ?>
            <table id="Table">
        </div>

    </div>
</body>
</html>

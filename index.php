<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>Lekarna</title> 
    <link rel="stylesheet" href="style.css" type="text/css" >
</head>

<body>

    <?php                             
        require_once 'mainInit.php';
    ?>

    <script>
        function showLIEK(url) {
            document.location.href=url;
        }

        function changeRowColor(item, hover) {
            if (hover) {
                item.className = 'TableRowHover';
            }
            else {
                item.className = 'TableRow';
            }
        }   
    </script>

    <div id="main">
        <?php include 'header.php' ?>

        <div id="content">
            <form action="prispevky.php?poist=0&liek=0">
                <input type="submit" value="Prispevky na leky" />
            </form>

            <form action="vsechny_rezervace.php">
                <input type="submit" value="Vsechny rezervace" />
            </form>

            <table id="Table">
                <tr>
                    <th>Nazov</th>
                    <th>Vyrobca</th>
                    <th>Predpis</th>
                    <th>Latky</th>
                </tr>

                <?php
                    $sql="SELECT LIEK.* FROM LIEK";
                    if (! $result = mysql_query($sql)){
                    die();
                    }

                    while($row = mysql_fetch_assoc($result)) {
                        echo '<tr class="TableRow" 
                                    onmouseover=\'changeRowColor(this, true)\' 
                                    onmouseout=\'changeRowColor(this, false)\' 
                                    onclick=\'showLIEK("lek.php?id='.$row['ID_LIEK'].'")\'
                                >';
                        echo '<td>' . $row['NAZOV'] . '</td>';
                        echo '<td>' . $row['VYROBCA'] . '</td>';

                        if($row['PREDPIS'] == 1)
                            echo '<td>' . "ano" . '</td>';
                        else
                            echo '<td>' . "ne" . '</td>';
                            echo '<td>' . $row['LATKY'] . '</td>';
                            echo '</tr>';
                    }
                ?>
            </table>
        </div>
    </div>
</body>
</html>

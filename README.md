# Informacny system pre Lekaren
Dnes jsem doplnil prodej, měl by fungovat – můžeš se mrknout. Aktualizuj si také skript sql v phpmyadmin – měnil jsem ho. Měl by ztroskotat až na triggeru, selecty jsem smazal.
Jinak tvá práce je v prispevky.php a všechny_rezervace.php, máš označené, kde bys měl doplnit kód. Z index.php je už na ně udělaný odkaz po kliknutí na tlačítko.
Jediné, co chybí je (citace zadání) : „import aktuálních hodnot příspěvků na léky od zdravotních pojišťoven (může se čas od času měnit).“



Statistiku příspěvků na léky uděláš jednoduše, akorát spojíš tabulky LIEK, DOPLATOK, POISTOVNA a vypíšeš potřebné věci. Selecty jsem dělal mnohokrát, takže se můžeš třeba podívat v index.php, tak vypisuji všechny léky…

Nevím, co s tou rezervací, asi to není v rámci zadání, ale když přijde zákazník, s tím, že má rezervovaný nějaký lék, měla by být možnost se podívat, zda tak opravdu je. Takže v podstatě jen vypíšeš celou tabulku rezervace a ke každému řádku bych přidal možnost „smazat“, která ten záznam odstraní z db (když si ho pak vyzvedne, tak se rezervace logicky ruší).

Pak kdyby se ti povedlo to přihlašování, tak by bylo super. Logicky bychom potřebovali novou tabulku s ID, loginem a heslem.

